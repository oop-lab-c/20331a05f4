#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private:
        int priVar;
    protected:
        int proVar;
    public:
      int pubVar;
    public:
    void setValue(int priValue,int proValue,int pubValue)// private public and protected values are set to some value
    {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }
     public:
    void getVar()// retrived those set values done in above function
    {
       cout<<priVar <<endl;
        cout<<proVar <<endl;
         cout<<pubVar <<endl;
    }
};
//those private and protected values can be used only in that class
int main()
{
     AccessSpecifierDemo obj;
     obj.setValue(15 ,35 ,45);
     obj.getVar();
     
     return 0;
}
