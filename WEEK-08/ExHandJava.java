// ArithmeticException
class ArithmeticException_Demo {
 void Arith()
    {
        try {
            int a = 30, b = 0;
            int c = a / b; // cannot divide by zero
            System.out.println("Result = " + c);
        }
        catch (ArithmeticException e) {
            System.out.println("Can't divide a number by 0");
        }
    }
}
// ArrayIndexOutOfBoundException
class ArrayIndexOutOfBound_Demo {
     void Arry()
        {
            try {
                int a[] = new int[5];
                a[6] = 9; // accessing 7th element in an array of
                // size 5
            }
            catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Array Index is Out Of Bounds");
            }
        }
    }
class Main
{    public static void main(String args[])
    {
        ArithmeticException_Demo obj1 = new ArithmeticException_Demo();
        ArrayIndexOutOfBound_Demo obj2 = new ArrayIndexOutOfBound_Demo();
        obj1.Arith();
        obj2.Arry();

    }}
