import java.util.Scanner;

class CalcJava
// main class
{
    public static void main(String[] args)
    {
        System.out.println("Enter two values : ");
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();//
        int b = input.nextInt();
        System.out.println("Enter the operator(+,-,*,/,%):");
        char c = input.next().charAt(0);
        if(c == '+')
        {
            System.out.println(a+b);
        }
        else if(c == '-')
        {
            System.out.println(a-b);
        }
        else if(c == '*')
        {
            System.out.println(a*b);
        }
        else if(c == '%')
        {
            if(b == 0)
            {
                System.out.println("Modulus operation is not possible\n");
            }
            else
            {
                System.out.println(a%b);
            }
        }
        else if(c == '/')
        {
            if(b == 0) 
            {
                System.out.println("Division operation is not possible!!\n");
            }
            else
            {
                System.out.println(a/b);
            }
        }
        else//
        {
            System.out.println("Enter valid operator");
        }
    }
}
