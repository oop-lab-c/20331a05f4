#include<iostream>
#include<string>
#include<iomanip>
using namespace std;
int main()
{
    string name;
    float x = 15.345;
    cout << setprecision(2) << x << endl;
    cout << setprecision(5) << x << endl;//gives 5 digits as output including decimal points
    cout<<setw(50)<<"rssr7773"<<endl;
    cout <<setw(15) << setfill('*') << 55 << 77 << endl;//15 *s
    cout<<"Enter Your name"<<endl;
    cin>>ws;//ws will ignore the blank spaces that are entered by the user before input value
    cin>>name;
    cout<<"Name is "<<name<<endl;
    cout<<"Ram & Lakshman"<<flush;// flush will flush the next line same as ends
    cout<<"hola"<<ends;//ends is used to skip next line and continues to print the next outputs immediately
    cout<<"Hello"<<endl;//endl is used to end the line on cout and print the next output from the next line..
}
