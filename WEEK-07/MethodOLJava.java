import java.util.*;

class MethodOLJava {
    void operation(int a, int b) {
        System.out.println(a + b);
    }

    void operation(int a, int b, int c) {
        System.out.println(a + b + c);
    }

    public static void main(String[] args) {
        MethodOLJava obj = new MethodOLJava();
        obj.operation(4, 6);
        obj.operation(4, 6, 4);

    }
}
