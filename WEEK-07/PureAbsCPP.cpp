#include<iostream>
using namespace std;
class shape{
    public:
    virtual void name()=0;
};
class Triangle : public shape{
    public:
    void name(){
        cout<<" triangle:3 sides"<<endl;
    }
};
class Square : public shape{
    public:
    void name(){
        cout<<"square:4 sides"<<endl;
    }
};
int main(){
    shape *b;
    Triangle obj;
    b = &obj;
    b->name();
    Square o;
    b = &o;
    b->name();
    return 0;
}
