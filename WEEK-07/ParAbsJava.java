abstract class Animal{
    abstract void walk();
    Animal(){
        System.out.println("Animal is\n");
    }
    public void talk(){
        System.out.println("Animal can talk\n");
    }
}
class Monkey extends Animal{
    public void walk(){
        System.out.println("Can Walk");
    }
    Monkey(){
        System.out.println("Monkey\n");
    }
}
class Main{
    public static void main(String[] args) {
        Monkey m = new Monkey();
        m.talk();
        m.walk(); 
    }
}
