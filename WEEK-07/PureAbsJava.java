interface Itface
{
    public void bark();
    public void walk();
}
class Dog implements Itface
{
    public void bark()
    {
        System.out.println("BOW... BOW\n");
    }
    public void walk()
    {
        System.out.println("Walk.");
    }
    public static void main(String[] args)
    {
        Dog a = new Dog();
        a.walk();
        a.bark();
    }
}
