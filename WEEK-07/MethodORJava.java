class Age{  
    int age(){return 0;}  
    }  
     
    class Father extends Age{  
    int age(){return 56;}  
    }  
    class Mother extends Age{  
    int age(){return 45;}  
    }  
    class Child extends Age{  
    int age(){return 21;}  
    }  
    class Main{  
    public static void main(String args[]){  
    Father f=new Father();  
    Mother m=new Mother();  
    Child me=new Child();  
    System.out.println("Father Age: "+f.age());  
    System.out.println("Mother Age: "+m.age());  
    System.out.println("My Age: "+me.age());  
    }  
    }  
    