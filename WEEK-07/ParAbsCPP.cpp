#include<iostream>
using namespace std;
class parAbs{
    public:
    int x,y;
    parAbs(int x, int y){
        this->x=x;
        this->y=y;
    }
    virtual int Sum()=0;
    void display(int S){
        cout<<"Sum ="<<S<<endl;
            }
};
class derived:public parAbs{
    public:
    
    derived(int x, int y):parAbs(x,y){};
    int Sum(){
        return x+y;
        
    }

};
int main(){
    int S;
    derived obj(15,17);
    S=obj.Sum();
    obj.display(S);
    return 0;

}
