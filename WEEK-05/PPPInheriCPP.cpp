#include<iostream>
using namespace std;
class Inheri
{
  public:
  int a=13;
  protected:
  int b=5;
  private:
  int c=7;
  public:
  void priv()  //function to access private
  {
      cout<<c<<endl;
  }
};
class publicInh : public Inheri
{
  public:
  //function to access protected member from base
  void prot()
  {
      cout<<b<<endl;
  }
};
class protectedInh : protected Inheri {
  public:
    // function to access protected member from Base
    void prot() {
      cout<<b<<endl;
    }
    // function to access public member from Base
    void publ() {
      cout<<a<<endl;
    }
};
class privateInh : private Inheri {
  public:
    // function to access protected member from Base
    void prot() {
      cout<<b<<endl;
    }

    // function to access private member
    void publ() {
      cout<<a<<endl;
    }
};
int main()
{
  publicInh object1;
  protectedInh object2;
  privateInh object3;
  cout << "Private = " << endl;
  object1.priv();
  cout << "Protected = "<< endl;
  object1.prot();
  cout << "Public = "<< endl;
  object1.a;
  
  cout<< "Private cannot be accessed."<< endl;
  cout << "Protected = " << endl;
   object2.prot();
  cout << "Public = " << endl;
  object2.publ();
  
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << endl;
  object3.prot();
  cout << "Public = "<< endl;
  object3.publ(); 
  return 0;
}
