import java.util.*;

class prvt {
    private void print1() {
        System.out.println(" I'm in private ");
    }
}

class pub {
    public void print2() {
        System.out.println("I'm public ");
    }
}

class prot {
    protected void print3() {
        System.out.println("I am protected");
    }
}

class PPPInheriJava{
    public static void main(String[] args) {
        prvt obj1 = new prvt();
        pub obj2 = new pub();
        prot obj3 = new prot();
        System.out.println("private\n");
       // obj1.print1();
       System.out.println("cannot be accessed");
        System.out.println("public\n");
        obj2.print2();
        System.out.println("protected\n");
        obj3.print3();
    }
}
