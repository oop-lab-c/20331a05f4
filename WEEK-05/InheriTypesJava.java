class Rain
{
    Rain()
    {
        System.out.println("I am Rain");
    }
}
class Lake extends Rain
{
    Lake()
    {
        System.out.println("I am Lake");
    }
}
class River extends Lake
{
    River()
    {
        System.out.println("I am River");
    }
}
class Sea extends River
{
    Sea()
    {
        System.out.println("I am Sea");
    }
}
class Ocean extends River
{
    Ocean()
    {
        System.out.println("I am Ocean");
    }
}
public class InheriTypesJava {
    public static void main(String args[])
    {
        System.out.println("  Single inheritance  ");
        Lake a=new Lake();
        System.out.println("  Multilevel inheritance  ");
        River b=new River();
        System.out.println("  Heirarchial inheritance  ");
        Sea c=new Sea();
        Ocean d= new Ocean();
    }
}

