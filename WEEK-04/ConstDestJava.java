class Student {
    public Student() {
        String fullName = "Ram";
        double semPercentage = 95.4;
        int rollNum = 25;
        String collegeName = "MVGR";
        int collegeCode = 35;
        System.out.println(fullName);
        System.out.println(semPercentage);
        System.out.println(rollNum);
        System.out.println(collegeName);
        System.out.println(collegeCode);
    }

    protected void finalize() {
        System.out.println("finalize method invoked");
    }

    public static void main(String[] args) {
        Student obj = new Student();
        obj.finalize();
    }

}